import json
import sys

def count_vulnerabilities_gl(file):
    with open(file, 'r') as f:
        data = json.load(f)
        if isinstance(data, list):
            return len(data)
        else:
            return len(data.get('Finding', []))

def count_vulnerabilities_sg(file):
    with open(file, 'r') as f:
        data = json.load(f)
        high_impact_vulnerabilities = 0
        for result in data.get('results', []):
            metadata = result.get('extra', {}).get('metadata', {})
            impact = metadata.get('impact')
            if impact == 'HIGH':
                high_impact_vulnerabilities += 1
        return high_impact_vulnerabilities

def count_vulnerabilities_ods(file):
    with open(file, 'r') as f:
        data = json.load(f)
        vulnerable_count = 0
        for dependency in data.get('dependencies', []):
            if 'vulnerabilities' in dependency and dependency['vulnerabilities']:
                vulnerable_count += 1
        return vulnerable_count

def count_vulnerabilities_ozap(file):
    with open(file, 'r') as f:
        data = json.load(f)
        vulnerabilities_count = 0
        for vulnerability in data.get('uri', []):
            vulnerabilities_count += 1
        return vulnerabilities_count

total_vulnerabilities = 0
total_vulnerabilities += count_vulnerabilities_gl('gitleaks-report.json')
total_vulnerabilities += count_vulnerabilities_sg('semgrep_results.json')
total_vulnerabilities += count_vulnerabilities_ods('dependency-check-report.json')
#print(f"Total vulnerabilities exceed 30: {total_vulnerabilities}")
# Uncomment the following line when 'ods.json' is available
#total_vulnerabilities += count_vulnerabilities_ozap('report_json.json')

if total_vulnerabilities > 1000:
    print(f"Total vulnerabilities exceed 1000: {total_vulnerabilities}")
    sys.exit(1)
else:
    print(f"Total vulnerabilities are within the limit: {total_vulnerabilities}")
    sys.exit(0)